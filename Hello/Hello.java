// logging
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hello {
    public static void main(String[] args) {
        Logger logger = Logger.getGlobal();
        System.out.println("Hello, world!");
        double x = Math.abs(-123.45);
        //double x = -123.45;
        assert x > 0;
        System.out.println(x);
        logger.info("start...");
        logger.warning("memo ran out.");
        logger.fine("ignored.");
        logger.severe("terminated.");
    }
}
